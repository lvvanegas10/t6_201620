package Test;

import junit.framework.TestCase;
import taller.estructuras.Lista;
import taller.estructuras.NodoHash;
import taller.estructuras.TablaHash;

public class HashTableTest  extends TestCase{

	// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Es la clase donde se harán las pruebas
		 */
		private TablaHash<Integer, String> tabla;

		// -----------------------------------------------------------------
		// Escenarios
		// -----------------------------------------------------------------

		/**
		 * Construye una nueva lista vacia
		 */
		private void setupEscenario1( )
		{
			tabla = new TablaHash<>(2, (float)0.8);
		}
		// -----------------------------------------------------------------
		// Métodos
		// -----------------------------------------------------------------
		/**
		 * Este método se encarga de verificar el método constructor de la clase<br>
		 * <b> Objetivo: </b> Probar que se inicialicen correctamente los atributos<br>
		 * <b> Resultados esperados: </b> <br>
		 * 1. El tamaño de la lista es 0<br>
		 * 2. La lista esta vacia <br>
		 */
		public void testAgregar( )
		{
			setupEscenario1();
			tabla.put(2, "a");
			tabla.put(3, "a");
			tabla.put(4, "a");
			tabla.put(5, "a");
			tabla.put(6, "a");
		
			Lista<NodoHash<Integer, String>>[] con = tabla.darNodos();
			try {
				assertEquals("a", con[0].darElementoPosicion(0).getValor());
				//assertEquals("Mal", 2, );
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}

		/**
		 * Este método se encarga de verificar los métodos agregarAlInicio, longitud y estaVacia de la clase<br>
		 * <b> Objetivo: </b> Probar que se agreguen correctamente los elementos<br>
		 * <b> Resultados esperados: </b> <br>
		 * 1. Los tres elementos se agregan correctamente al inicio de la lista "A" "B" "C"<br>
		 * 2. La lista no esta vacia y la longitud es la correcta después de agregar los elementos<br>
		 * 3. Si se pide un elemento en una posición fuera de rango se genera una excepción
		 */
		
}

package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private Lista<NodoHash<K, V>>[] contenedorTabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	public Lista<NodoHash<K, V>>[] darNodos()
	{
		return contenedorTabla;
	}
	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		count=0;
		capacidad=50;
		factorCargaMax = (float) 0.8;
		factorCarga=0;
		contenedorTabla = new Lista[capacidad];

		for (int i = 0; i < contenedorTabla.length; i++)
		{
			contenedorTabla[i] = new Lista<NodoHash<K, V>>();
		}
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacida, float factorCargaMa) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		count=0;
		capacidad=capacida;
		factorCargaMax = factorCargaMa;
		factorCarga=0;
		contenedorTabla = new Lista[capacidad];

		for (int i = 0; i < contenedorTabla.length; i++)
		{
			contenedorTabla[i] = new Lista<NodoHash<K, V>>();
		}		
	}

	public void put(K llave, V valor){
		//TODO: Guarde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int indice = hash(llave);
		NodoHash<K, V> nuevoNodo = new NodoHash<K, V>(llave, valor);
		contenedorTabla[ indice ].agregarAlFinal(nuevoNodo);
		count++;			
		rehash();
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones	
		NodoHash<K, V> a = buscar(llave);
		V res = null;
		if(a != null) res = a.getValor();
		return res;
	}

	public V delete(K llave) throws Exception{
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		NodoHash<K, V> bus = buscar(llave);
		V temp = null;
		if(bus!= null)
		{
			temp = bus.getValor();
			bus.setLlave(null);
			bus.setValor(null);
			contenedorTabla[hash(llave)].eliminarElemento(bus);
			count--;			
		}
		return temp;
				
	}

	public NodoHash<K, V> buscar(K llave)
	{
		int indice = hash(llave);
		Lista<NodoHash<K, V>> lista = contenedorTabla[indice];
		NodoHash<K, V> bus = null;
		for (int i = 0; i < lista.longitud(); i++) {
			try {
				if(lista.darElementoPosicion(i).getLlave().compareTo(llave)==0) bus= lista.darElementoPosicion(i);
			} catch (Exception e) {
				System.out.println("Estructura vacia: el elemento no existe");
			}
		}
		return bus;
	}
	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return  Math.abs(llave.hashCode()) % capacidad ;
	}

	//TODO: Permita que la tabla sea dinamica
	protected void rehash()
	{		
		int tam = contenedorTabla.length;
		factorCarga = count / tam;
		
		if (factorCarga >= factorCargaMax)
		{
			Lista<NodoHash<K, V>>[] anteriores = contenedorTabla;
			int nuevoTamanio = darPrimo( tam * 2 );
			contenedorTabla = new Lista[nuevoTamanio];
			
			for (int i = 0; i < contenedorTabla.length; i++)
			{
				contenedorTabla[i] = new Lista();
			}
			
					
			for (int i = 0; i < tam; i++)
			{
				Lista<NodoHash<K, V>> lista = anteriores[i];
				
				for (int j = 0; j < lista.longitud(); j++)
				{
					try {
						put(lista.darElementoPosicion(i).getLlave(), lista.darElementoPosicion(i).getValor());
					} catch (java.lang.Exception e) {
						//No entra
					}				
				}
			}
		}
	}
	
	private int darPrimo( int numero )
	{
		int primo = numero + 1;
		while( !esPrimo( primo ) )
		{
			primo++;
		}
		return primo;
	}
	
	private boolean esPrimo( int numero )
	{
		boolean primo = true;

		if( numero % 2 == 0 && numero != 2 )
		{
			primo = false;
		}
		else
		{
			int raiz = ( int )Math.sqrt( numero );

			for( int cont = 3; cont <= raiz && primo; cont += 2 )
			{
				if( numero % cont == 0 )
				{
					primo = false;
				}
			}
		}
		return primo;
	}
}
package taller.estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Lista<T>  {
	//--------------------------------------------------------
	// NODO: Reprenta un nodo de la lista
	//--------------------------------------------------------

	/**
	 * Clase que representa un nodo
	 */
	private class Nodo
	{
		T item;
		Nodo next;
	}

	//--------------------------------------------------------
	//ATRIBUTOS
	//--------------------------------------------------------
	/**
	 * Atributo del primer  elemento de la lista
	 */
	private Nodo cabeza = null;
	/**
	 * Atributo del primer  elemento de la lista
	 */
	private Nodo cola = null;
	/**
	 * Tamaño de la pila
	 */
	private int tamano=0;

	//--------------------------------------------------------
	//MÉTODOS
	//--------------------------------------------------------
	/**
	 * Permite agregar un elemento T al inicio de la colección
	 * @param elem Elemento tipo T a agregar
	 */
	public void agregarAlInicio(T elem)
	{
		Nodo primero = cabeza;
		cabeza = new Nodo();
		cabeza.item = elem;
		cabeza.next = primero;
		tamano++;
	}
	/**
	 * Permite agregar un elemento T al final de la colección
	 * @param elem Elemento tipo T a agregar
	 */
	public void agregarAlFinal(T elem)
	{
		Nodo ultimo= cola;
		cola = new Nodo();
		cola.item = elem;
		cola.next = null;
		if(estaVacia())	
			cabeza= cola;
		else
			ultimo.next= cola;
		tamano++;
	}
	/**
	 * Permite eliminar un elemento T a la colección
	 * @param pos Posición del elemento que se elimina
	 * @throws Exception si la estructura está vacia
	 */
	public T eliminarPrimero() throws Exception
	{
		if(estaVacia())
			throw new Exception("No hay elementos para eliminar");
		T item = cabeza.item;
		cabeza= cabeza.next;
		tamano--;
		if(estaVacia())
			cola= null;
		return item;
	}	

	/**
	 * Retorna el tamaño de la colección
	 * @return Tamaño de la colección
	 */
	public int longitud() { return tamano;}

	/**
	 * Indica si la colección está vacia 
	 * @return True si la colección esta vacia, False de lo contrario
	 */
	public boolean estaVacia() { return cabeza==null;}

	/**
	 * Retorna el nodo de la posición i
	 * @param i posición buscada
	 * @return ret nodo buscado 
	 * @throws EstructuraVaciaException si la estructura no tiene elementos
	 */
	public Nodo darNodoPosicion( int i ) throws Exception
	{
		Nodo aux = cabeza;
		if( i>= tamano)
			throw new Exception("Indice fuera de rango");
		else{	
			int contador=0;			
			while(iterator().hasNext() && contador!= i)
			{
				aux= aux.next;
				contador++;
				
			}
		}
		return aux;
	}

	/**
	 * Retorna el elemento que se encuentra en un posición dada
	 * @param i posición 
	 * @return	el elemento que se encuentra en unaposición dada
	 * @throws Exception si la estructura no tiene elementos o si el indice eta fuera de rango
	 */
	public T darElementoPosicion(int i) throws Exception 
	{
		return darNodoPosicion(i).item;
	}

	/**
	 * Permite eliminar un elemento T a la colección
	 * @param pos Posición del elemento que se elimina
	 * @throws Exception si la estructura está vacia
	 */
	public T eliminarElemento(T elemento) throws Exception
	{

		Nodo aux = cabeza;
		T re= null;
		if(estaVacia())
			throw new Exception("No hay elementos para eliminar");
		T item = cabeza.item;
		if(((Comparable<T>) item).compareTo(elemento)==0)
			re= eliminarPrimero();
		else
		{
			while(iterator().hasNext())
			{
				if(((Comparable<T>)aux.item).compareTo(elemento)==0)
				{
					re= aux.next.item;
					aux.next= aux.next.next;
					tamano--;
					break;
				}
			}
		}
		return re;
	}	


	//--------------------------------------------------------
	//ITERADOR
	//--------------------------------------------------------

	/**
	 * Iterador
	 * @return el iterador
	 */
	public Iterator<T> iterator()  { return new ListIterator();  }

	private class ListIterator implements Iterator<T> {
		private Nodo actual = cabeza;
		public boolean hasNext()  { return actual != null;                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public T next() {
			if (!hasNext()) throw new NoSuchElementException();
			T item = actual.item;
			actual = actual.next; 
			return item;
		}

	}
}
